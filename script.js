const elMinus = document.querySelectorAll('#packages button[minus]');
const elPlus = document.querySelectorAll('#packages button[plus]');
const elAction = document.querySelectorAll('#packages button[action]');

const elContactForm = document.querySelector('#contact form');

elMinus.forEach((el) => {
  el.addEventListener('click', (event) => {
    const { target: btn } = event;
    const { parentElement: para } = btn;

    para.parentElement.lastElementChild.style.display = 'none';

    const denominator = btn.value;
    const current = para.children[1].dataset.val;

    if (current === denominator) {
      return;
    }

    const now = +current - +denominator;

    para.children[1].dataset.val = now;
    para.children[1].innerHTML = `&dollar; ${now}`;
    para.children[3].innerHTML = `${now / denominator} room${
      now > denominator ? 's' : ''
    }`;
  });
});

elPlus.forEach((el) => {
  el.addEventListener('click', (event) => {
    const { target: btn } = event;
    const { parentElement: para } = btn;

    para.parentElement.lastElementChild.style.display = 'none';

    const denominator = btn.value;
    const current = para.children[1].dataset.val;

    const now = +current + +denominator;

    para.children[1].dataset.val = now;
    para.children[1].innerHTML = `&dollar; ${now}`;
    para.children[3].innerHTML = `${now / denominator} rooms`;
  });
});

elAction.forEach((el) => {
  el.addEventListener('click', (event) => {
    const { target: btn } = event;
    const para = btn.parentElement.previousElementSibling;

    const result = para.children[3].textContent;

    const output = para.parentElement.lastElementChild;
    output.innerHTML = `Thank you for choosing ${result}.`;
    output.style.display = 'block';
  });
});

elContactForm.addEventListener('submit', (event) => {
  event.preventDefault();

  const {
    name: _name,
    email: _email,
    message: _message,
  } = event.target.elements;
  const { value: name } = _name;
  const { value: email } = _email;
  const { value: message } = _message;

  if (!name || !email || !message) {
    return;
  }

  const output = elContactForm.nextElementSibling;
  output.lastElementChild.innerHTML = JSON.stringify({ name, email, message });
  output.style.display = 'block';
  elContactForm.reset();
  elContactForm.style.display = 'none';
});
